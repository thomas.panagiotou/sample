## Programm with all transformations for creating the 
#complexity calculations (Product and Country)
#Thomas Panagiotou 06/2022   
 ##########load data & packages& clean up###########
#Call packages
 #Load the required packages
 #manipulation from and to excel
library(readxl)
library(writexl)
 # used to compute matrix power
library(Matrix)
 library(expm)
library(MASS)
 ## read excel file (or write a path or do that via the menu)
data <- read_excel("C:/Users/thoma/Desktop/Technological Complexity/data/datasets/2set_40/2020_40.xlsx")
View(data)
# see the names of the variables
#verified
names(data)
#call the variables from the data frame exclude the first column
# see the dimension of the matrix of data
rwdata<-nrow(data)
cldata<-ncol(data)
#exclude the country names
names<-data[,1:1]
test<-data[,2:cldata]
#convert the data frame into a matrix
A<-data.matrix(test)
#### Calculations to produce the Mcp Matrix######
#set the dimension of the patents matrix 
#(always considered from now on)
m<-nrow(A)
n<-ncol(A)
#compute the sector patent sums and store into a row vector (nx1)
c<- colSums(A)
# compute the total number of patents
tot<-sum(c)
# convert the sector patent sums into a column vector(1xn)
ct <- t(c)
# set the sector patent sums as a data frame as well
dfc <-t(ct)
#create a nxm matrix of the sums (stack the same 1xn vector m times)
K1<-matrix( dfc , length(dfc) , m )
# see dimensions of K1 (must be nxm)
rK1<- nrow(K1)
cK1<- col(K1)
# Transpose K1 into a matrix K2 (mxn)
K2<-t(K1)
#create the data frame
K3<-data.frame(K2)
#compute the country patent sums and store into a column vector (1xm)
r<- rowSums(A)
#transpose into an 1xm vector
rt <- t(r)
dfr <- t(rt)
dfr1 <- data.frame(dfr)
dfr2 <- as.matrix(dfr1)
#create a m x n matrix of the sums (stack the vector 36 times)
L1a <- matrix(dfr2,length(dfr2),n) 
# alternatively compute L1a<- replicate(n, dfr2) gives same results 
#make this a data frame
L1<-data.frame(L1a)

###### Final computations to get the Mcp/RCA Matrix#####
# Multiply total patents to the patents matrix
C1<- A* tot
# multiply cell by cell L1 and K3
C2<- L1*K3
# Compute the ratio for all cells in the patents matrix
C3 <- C1/C2
# categorize for any element to be 1 above 1 and 0 otherwise
C4<- ifelse(C3>=1,1,0)
# Output of results
MCp<- data.frame(C4)

########compute Diversity########
div <- rowSums(C4)
#Create diversity into a diagonal matrix
D<- diag(div)
#take the inverse of the matrix
D_inv<- solve(D)

######compute Ubiquity######
ubi<- colSums(C4)
#Create Ubiquity into a diagonal matric
U<- diag(ubi)
#take the inverse of the matrix
U_inv<- solve(U)
#get the transpose of the MCp matrix
C4T <- t(C4)

######Compute MCC######
MCC<- D_inv %*% C4 %*% U_inv %*% C4T
test_MCC<- rowSums(MCC)
# In order to compute the eigenvectors/eigenvalues we have
#to transpose MCC so as the sum of the columns to be 1
MCCT<-t(MCC)
#Create data frames
dfMCC<-data.frame(MCC)
dfMCCT<-data.frame(MCCT)

######Compute MPP (same steps with MCC)######
MPP<- U_inv %*% C4T %*% D_inv %*% C4
test_MPP<- rowSums(MPP)
MPPT<-t(MPP)
dfMPP<-data.frame(MPP)
dfMPPT<-data.frame(MPPT)

####Generate stationary function ####
#This function  solves the system of equations in the 
#steady state and provides the steady state eigenvalues π 
stationary <- function(transition) {
  stopifnot(is.matrix(transition) &&
              nrow(transition)==ncol(transition) &&
              all(transition>=0 & transition<=1))
  p <- diag(nrow(transition)) - transition
  B <- rbind(t(p),
             rep(1, ncol(transition)))
  b <- c(rep(0, nrow(transition)),
         1)
  res <- qr.solve(B, b)
  names(res) <- paste0("state.", 1:nrow(transition))
  return(res)
}

################# MCC compute steady state probability matrix#############

### 1. Method Power method###
#Try the brute force method to calculate THE ERGODIC 
#pi matrix
# Multiply the MCC 100 times with itself MCC^(100)
# and get the 2nd column as an eigenvector
pi_bru_MCC2<- (MCCT %^% 100)[,2]
#make this a data frame
vec_bru_MCC2<-as.data.frame(pi_bru_MCC2)
#sum probabilities to see result
colSums(vec_bru_MCC2)
#get mean and standard deviation
mu_bru_MCC2<-mean(pi_bru_MCC2)
sd_bru_MCC2<-sd(pi_bru_MCC2)
# standardize the eigenvector
cmplxty_bru_MCC<- (vec_bru_MCC2-(rep(1,m)*mu_bru_MCC2))/sd_bru_MCC2
#compute the whole matrix
pi_bru_MCC<- (MCCT %^%  100)

###### 2. Method solve system####
#compute the MCC eigenvector via the function
#Solve the system of augmented equations
#Use the function for MCC (transposed) and store the eigenvector
vectMCC<-stationary(matrix(MCCT,nrow=m, byrow=TRUE))
# make the vector a data frame
complx1<- as.data.frame(vectMCC)
#check if the probabilities sum to 1
colSums(complx1)
#compute the mean and the standard deviation
muT<-mean(vectMCC)
sT<-sd(vectMCC)
#Create the index
#generate a vector of ones
ones<- rep(1,m)
#standardize
cmplx_Country_Ind<- (complx1-(ones*muT))/sT
cmplx_Country_Ind_output<- cbind.data.frame(names,cmplx_Country_Ind,complx1,cmplxty_bru_MCC,vec_bru_MCC2)
###3. Method QR Decomposition#####
#E1<- rbind(MCCT,t(rep(1,m)))
#E<-matrix(E1,ncol=ncol(E1),nrow = nrow(E1))
#b1<-t(rep(0,m))
#b2<-cbind(b1,1)
#b<- t(b2)
#pi_QR <- qr.solve(E,b)
#store all results into a data frame
 
### Method 4 Eigendecomposition####
# Get the eigenvectors of P, note: R returns right eigenvectors
#eig_dec_MCC=eigen(E1)
#rvec=eig_dec_MCC$vectors
#lvec=ginv(eig_dec_MCC$vectors)
#lam<-eig_dec_MCC$values
#rvec%*%diag(lam)%*%ginv(rvec)

################# MPP compute steady state probability matrix#############

### 1. Method Power method###
#Try the brute force method to calculate THE ERGODIC 
#pi matrix
# Multiply the MCC 100 times with itself MPP^(100)
# and get the 2nd column as an eigenvector
pi_bru_MPP2<- (MPPT %^% 100)[,2]
#make this a data frame
vec_bru_MPP2<-as.data.frame(pi_bru_MPP2)
#sum probabilities to see result
colSums(vec_bru_MPP2)
#get mean and standard deviation
mu_bru_MPP2<-mean(pi_bru_MPP2)
sd_bru_MPP2<-sd(pi_bru_MPP2)
# standardize the eigenvector
cmplxty_bru_MPP<- (vec_bru_MPP2-(rep(1,n)*mu_bru_MPP2))/sd_bru_MPP2
#compute the whole matrix
pi_bru_MPP<- (MPPT %^%  100)

###### 2. Method solve system####
#compute the MCC eigenvector via the function
#Solve the system of augmented equations
#Use the function for MCC (transposed) and store the eigenvector
vectMPP<-stationary(matrix(MPPT,nrow=n, byrow=TRUE))
# make the vector a data frame
complx1a<- as.data.frame(vectMPP)
#check if the probabilities sum to 1
colSums(complx1a)
#compute the mean and the standard deviation
muP<-mean(vectMPP)
sP<-sd(vectMPP)
#Create the index
  #generate a vector of ones
  ones<- rep(1,n)
#standardize
cmplx_product_Ind<- (complx1a-(ones*muP))/sP
cmplx_product_Ind_output<- cbind.data.frame(cmplx_product_Ind,complx1a,cmplxty_bru_MPP,vec_bru_MPP2)
###3. Method QR Decomposition#####
#E1<- rbind(MCCT,t(rep(1,m)))
#E<-matrix(E1,ncol=ncol(E1),nrow = nrow(E1))
#b1<-t(rep(0,m))
#b2<-cbind(b1,1)
#b<- t(b2)
#pi_QR <- qr.solve(E,b)

### Method 4 Eigendecomposition####
# Get the eigenvectors of P, note: R returns right eigenvectors
#eig_dec_MCC=eigen(E1)
#rvec=eig_dec_MCC$vectors
#lvec=ginv(eig_dec_MCC$vectors)
#lam<-eig_dec_MCC$values
#rvec%*%diag(lam)%*%ginv(rvec)

#Save results
#save the 2nd matrix transformation
write_xlsx(C3, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\2ndtable_40_2020.xlsx")

#save the rca matrix transformation
write_xlsx(MCp, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\RCA_40_2020.xlsx")
#save the MPP matrix transformation
write_xlsx(dfMPP, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\MPP_40_2020.xlsx")
#save the MCC matrix transformation
write_xlsx(dfMCC, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\MCC_40_2020.xlsx")
#save the Complexity country estimates
write_xlsx(cmplx_Country_Ind_output, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\cmplx_Country_Ind_output.xlsx")
#save the Complexity product estimates
write_xlsx(cmplx_product_Ind_output, "C:\\Users\\thoma\\Desktop\\Technological Complexity\\data\\cmplx_product_Ind_output.xlsx")

